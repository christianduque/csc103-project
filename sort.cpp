#include <cstdio>
#include <cctype>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <map>

using std::string;
#include <iostream>
#include <cstring>
using std::cin;
using std::cout;
#include <set>
using std::set;
using std::multiset;
#include <strings.h>
#include <vector>
using namespace std;
static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct igncaseComp {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */
void printRF2 (map <string , int , igncaseComp> rf)
{
for (map<string, int, igncaseComp>:: reverse_iterator rit2 = rf.rbegin(); rit2 != rf.rend(); rit2 ++)
	{	int temp= rit2->second;
		while (temp>0)
		{
		cout<<rit2->first<<endl;
			temp--;
		}

	}

}
void printRFU (set <string, igncaseComp>  set3)
{

		typedef set <string> :: reverse_iterator rit;
		rit riterator;

for (riterator= set3.rbegin(); riterator != set3.rend(); ++riterator)
    {
        cout<<*riterator<<endl;

    }
}
void printRF (set <string, igncaseComp>  mset)
{
	typedef set <string>:: iterator it;
	vector<string> please;
    it iterator;
    for (iterator= mset.begin(); iterator != mset.end(); iterator++)
    {

				please.push_back(*iterator);

    }

		string max,temp;
	int max_pos=0;

	for (int i= 0; i<please.size() ; i++)
	 {
			max = please[i];
		  max_pos=i;

		 for (int j = i ; j<please.size(); j++)
		 {
			 if (please[j]>max)// found maximum
				 {
					 max=please[j];
					 max_pos=j;

				 }
				 else {}
		 }
			// swap words[i] with words[max_position]
		 temp = please[i];
		 please[i] = please[max_pos];
		 please[max_pos]= temp;
		}

		for (int i= 0 ; i<please.size(); i++)
		{
				cout<<please[i]<<endl;
		}
	}





void printMS (multiset<string,igncaseComp >   mset)
{

		typedef set <string>:: iterator it;
    it iterator;
    for (iterator= mset.begin(); iterator != mset.end(); iterator++)
    {
        cout<<*iterator<<endl;

    }

}

void printS(set<string,igncaseComp> names)
{
    typedef set <string>:: iterator it;
    it iterator;
    for (iterator= names.begin(); iterator != names.end(); iterator++)
    {
        cout<<*iterator<<endl;

    }

}

void printRS(set<string> rnames)
{
    typedef set <string>:: iterator it;
    it iterator;
    for (iterator= rnames.begin(); iterator != rnames.end(); iterator++)
    {
        cout<<*iterator<<endl;

    }

}


void printV (vector <string> words)
{
	for (int i = 0; i<words.size(); i++)
		{
				cout<<words[i]<<endl;

		}

}

void reverse(vector <string> & words, set<string> &rwords)
{

	string max,temp;
	int max_pos=0;

	for (int i= 0; i<words.size() ; i++)
	 {
			max = words[i];
		  max_pos=i;

		 for (int j = i ; j<words.size(); j++)
		 {
			 if (words[j]>max)// found maximum
				 {
					 max=words[j];
					 max_pos=j;

				 }
				 else {}
		 }
			// swap words[i] with words[max_position]
		 temp = words[i];
		 words[i] = words[max_pos];
		 words[max_pos]= temp;
		}


	}




int main(int argc, char *argv[]) {
	set <string> rwords;
	set <string,igncaseComp> fwords;
	multiset<string,igncaseComp >  mset;
	multiset<string , greater <string> > mset2;
	map < string , int, igncaseComp> rf;



	vector <string> words; // define vector
  string word; // define variable that will store my string
  while (getline(cin,word))
  {

      words.push_back(word);
			rwords.insert(word);
			fwords.insert(word);
			mset.insert(word);
			mset2.insert(word);
			rf[word]++;

  }

	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				reverse (words,rwords);
				break;
			case 'f':
				ignorecase = 1;

				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}


if ( ignorecase != 1 &&  descending != 1 && unique != 1)
{
   printRS(rwords);

}

if ( (descending==1 && unique ==1) && (ignorecase !=1) )
{

	printV(words);

}

if ((unique==1 && ignorecase ==1) && descending != 1)
	{
		printS(fwords);

	}


if ((unique ==1 )&& (ignorecase!=1 && descending !=1))
{
	printRS(rwords);

}

if ((ignorecase ==1 )&& (unique!=1 && descending !=1))
{
	printMS(mset);

}

if ((descending==1 && ignorecase ==1) && unique != 1)
	{
	printRF2(rf);

	}

if ( (descending==1) && (ignorecase !=1 && unique !=1) )
{

	printV(words);

}
if ( descending==1 && ignorecase ==1 && unique ==1){
	printRFU(fwords);
}





	return 0;
}