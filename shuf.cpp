#include <cstdio>   // printf
#include <cstdlib>  // rand
#include <time.h>   // time
#include <getopt.h> // to parse long arguments.
#include <stdlib.h>
#include <string>
using std::string;
using std::stoi;
#include <vector>
using std::vector;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <algorithm>
using std::find;
using std::swap;
using std::min;


static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of shuf.  Supported options:\n\n"
"   -e,--echo              treat each argument as an input line.\n"
"   -i,--input-range=LO-HI treat each number in [LO..HI] as an input line.\n"
"   -n,--head-count=N      output at most N lines.\n"
"   --help                 show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int echo=0, rlow=0, rhigh=0;
	static size_t count=-1;
	bool userange = false;
	static struct option long_opts[] = {
		{"echo",        no_argument,       0, 'e'},
		{"input-range", required_argument, 0, 'i'},
		{"head-count",  required_argument, 0, 'n'},
		{"help",        no_argument,       0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "ei:n:h", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'e':
				echo = 1;
				break;
			case 'i':
				if (sscanf(optarg,"%i-%i",&rlow,&rhigh) != 2) {
					fprintf(stderr, "Format for --input-range is N-M\n");
					rlow=0; rhigh=-1;
				} else {
					userange = true;
				}
				break;
			case 'n':
				count = atol(optarg);
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/* NOTE: the system's shuf does not read stdin *and* use -i or -e.
	 * Even -i and -e are mutally exclusive... */

	if (echo && userange && rhigh!=-1){
		cout<<"\nshuf: cannot combine -e and -i options\nTry 'shuf --help' for more information."<<endl;
	return 0;
	}
	srand(time(0));
	if (userange==0) {
		//no args, or just -n, optional echo
		string line;
		vector<string> lines;
		if (echo) {
			string s;
			int i;
			if (count==-1) {i=2;}
			else {i=4;}
			while (i<argc) {
				s = argv[i];
				lines.push_back(s);
				++i;
			}
		}
		else {
			while (getline(cin,line)) {
				lines.push_back(line);
			}
		}
		vector<int> randNums;
		while (randNums.size()<lines.size()){
			int randNum = rand()%lines.size();
			if (find(randNums.begin(),randNums.end(),randNum) == randNums.end()){
				randNums.push_back(randNum);
			}
		}
		int prints;
		if (count==-1) {prints=lines.size();}
		else {prints=count;}
		for (int i=0; i<prints; ++i) {
			cout<<lines[randNums[i]]<<endl;
		}
		return 0;
	}	
	else {
		// userange with and without -n
		int lowi, highi, prints;
		string low="", high="";
		if (count != -1) {
			if (argv[1][1]=='i') {
				if (argc==5) {
					int i=0;
					while (argv[2][i] != '-') {
						low += argv[2][i];
						++i;
					}
					++i;
					while (i<sizeof(argv[2])) {
						high += argv[2][i];
						++i;
					}
					lowi=stoi(low);
					highi=stoi(high);
				}
				else {
					int i=2;
					while (argv[1][i] != '-') {
						low += argv[1][i];
						++i;
					}
					++i;
					while (i<sizeof(argv[1])) {
						high += argv[1][i];
						++i;
					}
					lowi=stoi(low);
					highi=stoi(high);
				}
			}
			else {
				if (argc==5) {
					int i=0;
					while (argv[4][i] != '-') {
						low += argv[4][i];
						++i;
					}
					++i;
					while (i<sizeof(argv[4])) {
						high += argv[4][i];
						++i;
					}
					lowi=stoi(low);
					highi=stoi(high);
				}
				else {
					int i=2;
					while (argv[3][i] != '-') {
						low += argv[3][i];
						++i;
					}
					++i;
					while (i<sizeof(argv[3])) {
						high += argv[3][i];
						++i;
					}
					lowi=stoi(low);
					highi=stoi(high);
				}
			}
			prints=count;
		}
		else {
			if (argc==3) {
				int i=0;
				while (argv[2][i] != '-') {
					low += argv[2][i];
					++i;
				}
				++i;
				while (i<sizeof(argv[2])) {
					high += argv[2][i];
					++i;
				}
				lowi=stoi(low);
				highi=stoi(high);
			}
			else {
				int i=2;
				while (argv[1][i] != '-') {
					low += argv[1][i];
					++i;
				}
				++i;
				while (i<sizeof(argv[1])) {
					high += argv[1][i];
					++i;
				}
				lowi=stoi(low);
				highi=stoi(high);
			}
			prints = highi-lowi+1;
		}
		if (lowi>highi) {cout<<"shuf: invalid input range: '"<<lowi<<"-"<<highi<<"'"<<endl; return 0;}
		vector<int> randNums;
		while (randNums.size()<highi-lowi+1) {
			int randNum = rand()%(highi-lowi+1)+lowi;
			if (find(randNums.begin(),randNums.end(), randNum)==randNums.end()){
					randNums.push_back(randNum);
			}
		}
		for (int i=0; i<prints && i<randNums.size(); ++i) {
			cout<<randNums[i]<<endl;
		}	
		//cout<<endl;
		return 0;
	}

	return 0;
}
