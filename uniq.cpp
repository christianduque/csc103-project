#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
#include <string.h>
#include <vector>
using std::string;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
using namespace std;


void print_c (){

	string prev_str, str, count_str;
	size_t count=1;
	getline(cin,prev_str);

	while(getline(cin,str)){
		if(prev_str == str){
			count++;
			prev_str = str;
		}
		else if(prev_str != str){
			count_str = prev_str;
			prev_str = str;
			if(count>9){
			cout<< "     " << count << " " << count_str <<endl;
			}else{
				cout<< "      " << count << " " << count_str <<endl;
			}
			count =1;
		}
	}
	cout<< "      " << count << " " << prev_str <<endl;
}


void print_d (){

	string prev_str, str;
	string to_print = "zarandom";
	size_t count=0;
	getline(cin,prev_str);

	while(getline(cin,str)){
		if(prev_str == str && str != to_print){
			prev_str=str;
			count++;
		}
		to_print = prev_str;
		if(count > 0){
			cout<<to_print <<endl;
    }
		prev_str =str;
		count =0;
	}
}

void print_u (){

	string word,past;
  vector <string> words;
  int change, count;


  while(getline(cin,word)){words.push_back(word);}

  for(int i=1; i<words.size(); i++)
  {
    count=0;
    for(int j=i; j<words.size(); j++)
    {
      if (words[i] != words[j+1])
      {
        change=1;
        past = words[i];
        break;
      }
      else {change = 0;}
    }
    if ((change == 1) && (past != words[i-1]) && (past != words[i+1]))
    {
    cout << past << endl;
    }
  }
}

void print_uniq (){

	string prev_str, str, to_print;
	size_t count=0;
	getline(cin,prev_str);

	while(getline(cin,str)){
		if(prev_str == str){
			prev_str=str;
		}
		else{
			cout<<prev_str <<endl;
			prev_str = str;
		}
	}
	cout<<prev_str <<endl;
}

void print_c_d (){

	string prev_str, str, count_str;
	size_t count=1;
	getline(cin,prev_str);

	while(getline(cin,str)){
		if(prev_str == str){
			count++;
			prev_str = str;
		}
		else if(prev_str != str){
			count_str = prev_str;
			prev_str = str;

			if((count > 1) && (count<9)){
				cout<< "      " << count << " " << count_str <<endl;
				count =1;
			}
			else if(count>9){
				cout<< "     " << count << " " << count_str <<endl;
				count =1;
			}
		}
	}
	if( (count > 1) && (count < 9) ){
		cout<< "      " << count << " " << prev_str <<endl;
	}
	else if (count> 9){
		cout<< "     " << count << " " << count_str <<endl;
	}
}



void print_d_u (){
	// nothing happened
}

void print_c_u (){ //count how many are uniqes to finish

	string word,past;
  vector <string> words;
  int change, count;
	size_t number=0;


  while(getline(cin,word)){words.push_back(word);}

  for(int i=1; i<words.size(); i++)
  {
    count=0;
    for(int j=i; j<words.size(); j++)
    {
      if (words[i] != words[j+1])
      {
        change=1;
        past = words[i];
        break;
      }
      else {change = 0;}
    }
    if ((change == 1) && (past != words[i-1]) && (past != words[i+1]))
    {
		cout<< "      1 " <<past <<endl;

    }
  }
}

void print_c_d_u (){
	// nothing happened
}



static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if ((showcount == 1) && (dupsonly != 1) && (uniqonly != 1)){
		print_c();
	}

	if ((showcount != 1) && (dupsonly == 1) && (uniqonly != 1)){
		print_d();
	}

	if ((showcount != 1) && (dupsonly != 1) && (uniqonly == 1)){
		print_u();
	}

	if ((showcount == 1) && (dupsonly == 1) && (uniqonly != 1)){
		print_c_d();

	}

	if ((showcount != 1) && (dupsonly == 1) && (uniqonly == 1)){
		print_d_u ();

	}

	if ((showcount == 1) && (dupsonly != 1) && (uniqonly == 1)){
		print_c_u ();
	}

	if ((showcount == 1) && (dupsonly == 1) && (uniqonly == 1)){
		print_c_d_u ();
	}

	if ((showcount != 1) && (dupsonly != 1) && (uniqonly != 1)){
		print_uniq();
	}

	return 0;
}
