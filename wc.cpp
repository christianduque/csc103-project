/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 
 */

#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iostream>
using std::cout;
using std::endl;
using std::cin;
using std::getline;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (charonly==0 && linesonly==0 && wordsonly==0 && uwordsonly==0 && longonly==0){
	charonly=1;linesonly=1;wordsonly=1;
	}
	int lines=0, bytes, totalBytes=0, words=0, maxLength=0;
	string line;
	set <string> uwords;
	while (getline(cin,line)){
		++lines;
		bytes = line.size()+1;
		totalBytes += bytes; 
		int i, length=0;
		for (i=0; i<bytes-1; ++i){
			if (line[i]=='\t'){
				//cout<<"TAB"<<endl;
				length += 8-length%8;
			}
			else {++length;}
		}
		if (length>maxLength){maxLength=length;}
		i=0;
		while(i<bytes-1){
			string word="";
			while (line[i]==' ' && i<bytes-1){++i;}
			while (line[i]!=' ' && i<bytes-1){word += line[i++];}
			cout<<word<<" ";
			if (word.size()){uwords.insert(word); ++words;}
		}
	}
	cout<<endl;
	if (linesonly){cout<<lines<<"   ";}
	if (wordsonly){cout<<words<<"   ";}
	if (charonly){cout<<totalBytes<<"   ";}
	if (uwordsonly){cout<<uwords.size()<<"   ";}
	if (longonly){cout<<maxLength<<"   ";}
	cout<<endl;
	return 0;
}
